Feature: Case Creation

    As a member of the enquiries team to manage properties
    I want to create new cases to manage my workload, track progress
    against them, and record important information.

    Scenario: Create case from a phone
        Given I am a member of the 'Enquiries team'
        And I am creating a case
        And I set the 'CaseOrigin' to 'Phone'
        When I save
        Then the 'CaseOrigin' is 'Phone'

    Scenario: Create case from an email
        Given I am a member of the 'Enquiries team'
        And I am creating a case
        And I set the 'CaseOrigin' to 'Email'
        When I save
        Then the 'CaseOrigin' is 'Email'
