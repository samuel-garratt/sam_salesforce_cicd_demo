# frozen_string_literal: true

require_relative 'account_field_names'
# An Account object mapping to a SOQL Account
class Account < SoqlData
  include Account::Fields
end
