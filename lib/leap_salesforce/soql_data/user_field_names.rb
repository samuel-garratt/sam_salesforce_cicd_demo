# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Fields for User mapped from SOQL User
class User < SoqlData
  module Fields
    extend SoqlGlobalObjectData

    # Element for 'User ID', type 'id'
    soql_element :user_id, 'Id'

    # Element for 'Username', type 'string'
    soql_element :username, 'Username'

    # Element for 'Last Name', type 'string'
    soql_element :last_name, 'LastName'

    # Element for 'First Name', type 'string'
    soql_element :first_name, 'FirstName'

    # Element for 'Full Name', type 'string'
    soql_element :full_name, 'Name'

    # Element for 'Company Name', type 'string'
    soql_element :company_name, 'CompanyName'

    # Element for 'Division', type 'string'
    soql_element :division, 'Division'

    # Element for 'Department', type 'string'
    soql_element :department, 'Department'

    # Element for 'Title', type 'string'
    soql_element :title, 'Title'

    # Element for 'Street', type 'textarea'
    soql_element :street, 'Street'

    # Element for 'City', type 'string'
    soql_element :city, 'City'

    # Element for 'State/Province', type 'string'
    soql_element :state_province, 'State'

    # Element for 'Zip/Postal Code', type 'string'
    soql_element :zip_postal_code, 'PostalCode'

    # Element for 'Country', type 'string'
    soql_element :country, 'Country'

    # Element for 'Latitude', type 'double'
    soql_element :latitude, 'Latitude'

    # Element for 'Longitude', type 'double'
    soql_element :longitude, 'Longitude'

    # Element for 'Geocode Accuracy', type 'picklist'
    soql_element :geocode_accuracy, 'GeocodeAccuracy'

    # Element for 'Address', type 'address'
    soql_element :address, 'Address'

    # Element for 'Email', type 'email'
    soql_element :email, 'Email'

    # Element for 'AutoBcc', type 'boolean'
    soql_element :auto_bcc, 'EmailPreferencesAutoBcc'

    # Element for 'AutoBccStayInTouch', type 'boolean'
    soql_element :auto_bcc_stay_in_touch, 'EmailPreferencesAutoBccStayInTouch'

    # Element for 'StayInTouchReminder', type 'boolean'
    soql_element :stay_in_touch_reminder, 'EmailPreferencesStayInTouchReminder'

    # Element for 'Email Sender Address', type 'email'
    soql_element :email_sender_address, 'SenderEmail'

    # Element for 'Email Sender Name', type 'string'
    soql_element :email_sender_name, 'SenderName'

    # Element for 'Email Signature', type 'textarea'
    soql_element :email_signature, 'Signature'

    # Element for 'Stay-in-Touch Email Subject', type 'string'
    soql_element :stayin_touch_email_subject, 'StayInTouchSubject'

    # Element for 'Stay-in-Touch Email Signature', type 'textarea'
    soql_element :stayin_touch_email_signature, 'StayInTouchSignature'

    # Element for 'Stay-in-Touch Email Note', type 'string'
    soql_element :stayin_touch_email_note, 'StayInTouchNote'

    # Element for 'Phone', type 'phone'
    soql_element :phone, 'Phone'

    # Element for 'Fax', type 'phone'
    soql_element :fax, 'Fax'

    # Element for 'Mobile', type 'phone'
    soql_element :mobile, 'MobilePhone'

    # Element for 'Alias', type 'string'
    soql_element :my_alias, 'Alias'

    # Element for 'Nickname', type 'string'
    soql_element :nickname, 'CommunityNickname'

    # Element for 'User Photo badge text overlay', type 'string'
    soql_element :user_photo_badge_text_overlay, 'BadgeText'

    # Element for 'Active', type 'boolean'
    soql_element :active, 'IsActive'

    # Element for 'Time Zone', type 'picklist'
    soql_element :time_zone, 'TimeZoneSidKey'

    # Element for 'Role ID', type 'reference'
    soql_element :role_id, 'UserRoleId'

    # Element for 'Locale', type 'picklist'
    soql_element :locale, 'LocaleSidKey'

    # Element for 'Info Emails', type 'boolean'
    soql_element :info_emails, 'ReceivesInfoEmails'

    # Element for 'Admin Info Emails', type 'boolean'
    soql_element :admin_info_emails, 'ReceivesAdminInfoEmails'

    # Element for 'Email Encoding', type 'picklist'
    soql_element :email_encoding, 'EmailEncodingKey'

    # Element for 'Profile ID', type 'reference'
    soql_element :profile_id, 'ProfileId'

    # Element for 'User Type', type 'picklist'
    soql_element :user_type, 'UserType'

    # Element for 'Language', type 'picklist'
    soql_element :language, 'LanguageLocaleKey'

    # Element for 'Employee Number', type 'string'
    soql_element :employee_number, 'EmployeeNumber'

    # Element for 'Delegated Approver ID', type 'reference'
    soql_element :delegated_approver_id, 'DelegatedApproverId'

    # Element for 'Manager ID', type 'reference'
    soql_element :manager_id, 'ManagerId'

    # Element for 'Last Login', type 'datetime'
    soql_element :last_login, 'LastLoginDate'

    # Element for 'Last Password Change or Reset', type 'datetime'
    soql_element :last_password_change_or_reset, 'LastPasswordChangeDate'

    # Element for 'Created Date', type 'datetime'
    soql_element :created_date, 'CreatedDate'

    # Element for 'Created By ID', type 'reference'
    soql_element :created_by_id, 'CreatedById'

    # Element for 'Last Modified Date', type 'datetime'
    soql_element :last_modified_date, 'LastModifiedDate'

    # Element for 'Last Modified By ID', type 'reference'
    soql_element :last_modified_by_id, 'LastModifiedById'

    # Element for 'System Modstamp', type 'datetime'
    soql_element :system_modstamp, 'SystemModstamp'

    # Element for 'Offline Edition Trial Expiration Date', type 'datetime'
    soql_element :offline_edition_trial_expiration_date, 'OfflineTrialExpirationDate'

    # Element for 'Sales Anywhere Trial Expiration Date', type 'datetime'
    soql_element :sales_anywhere_trial_expiration_date, 'OfflinePdaTrialExpirationDate'

    # Element for 'Marketing User', type 'boolean'
    soql_element :marketing_user, 'UserPermissionsMarketingUser'

    # Element for 'Offline User', type 'boolean'
    soql_element :offline_user, 'UserPermissionsOfflineUser'

    # Element for 'Auto-login To Call Center', type 'boolean'
    soql_element :autologin_to_call_center, 'UserPermissionsCallCenterAutoLogin'

    # Element for 'Apex Mobile User', type 'boolean'
    soql_element :apex_mobile_user, 'UserPermissionsMobileUser'

    # Element for 'Salesforce CRM Content User', type 'boolean'
    soql_element :salesforce_crm_content_user, 'UserPermissionsSFContentUser'

    # Element for 'Knowledge User', type 'boolean'
    soql_element :knowledge_user, 'UserPermissionsKnowledgeUser'

    # Element for 'Flow User', type 'boolean'
    soql_element :flow_user, 'UserPermissionsInteractionUser'

    # Element for 'Service Cloud User', type 'boolean'
    soql_element :service_cloud_user, 'UserPermissionsSupportUser'

    # Element for 'Data.com User', type 'boolean'
    soql_element :datacom_user, 'UserPermissionsJigsawProspectingUser'

    # Element for 'Site.com Contributor User', type 'boolean'
    soql_element :sitecom_contributor_user, 'UserPermissionsSiteforceContributorUser'

    # Element for 'Site.com Publisher User', type 'boolean'
    soql_element :sitecom_publisher_user, 'UserPermissionsSiteforcePublisherUser'

    # Element for 'Work.com User', type 'boolean'
    soql_element :workcom_user, 'UserPermissionsWorkDotComUserFeature'

    # Element for 'Allow Forecasting', type 'boolean'
    soql_element :allow_forecasting, 'ForecastEnabled'

    # Element for 'ActivityRemindersPopup', type 'boolean'
    soql_element :activity_reminders_popup, 'UserPreferencesActivityRemindersPopup'

    # Element for 'EventRemindersCheckboxDefault', type 'boolean'
    soql_element :event_reminders_checkbox_default, 'UserPreferencesEventRemindersCheckboxDefault'

    # Element for 'TaskRemindersCheckboxDefault', type 'boolean'
    soql_element :task_reminders_checkbox_default, 'UserPreferencesTaskRemindersCheckboxDefault'

    # Element for 'ReminderSoundOff', type 'boolean'
    soql_element :reminder_sound_off, 'UserPreferencesReminderSoundOff'

    # Element for 'DisableAllFeedsEmail', type 'boolean'
    soql_element :disable_all_feeds_email, 'UserPreferencesDisableAllFeedsEmail'

    # Element for 'DisableFollowersEmail', type 'boolean'
    soql_element :disable_followers_email, 'UserPreferencesDisableFollowersEmail'

    # Element for 'DisableProfilePostEmail', type 'boolean'
    soql_element :disable_profile_post_email, 'UserPreferencesDisableProfilePostEmail'

    # Element for 'DisableChangeCommentEmail', type 'boolean'
    soql_element :disable_change_comment_email, 'UserPreferencesDisableChangeCommentEmail'

    # Element for 'DisableLaterCommentEmail', type 'boolean'
    soql_element :disable_later_comment_email, 'UserPreferencesDisableLaterCommentEmail'

    # Element for 'DisProfPostCommentEmail', type 'boolean'
    soql_element :dis_prof_post_comment_email, 'UserPreferencesDisProfPostCommentEmail'

    # Element for 'ContentNoEmail', type 'boolean'
    soql_element :content_no_email, 'UserPreferencesContentNoEmail'

    # Element for 'ContentEmailAsAndWhen', type 'boolean'
    soql_element :content_email_as_and_when, 'UserPreferencesContentEmailAsAndWhen'

    # Element for 'ApexPagesDeveloperMode', type 'boolean'
    soql_element :apex_pages_developer_mode, 'UserPreferencesApexPagesDeveloperMode'

    # Element for 'HideCSNGetChatterMobileTask', type 'boolean'
    soql_element :hide_csn_get_chatter_mobile_task, 'UserPreferencesHideCSNGetChatterMobileTask'

    # Element for 'DisableMentionsPostEmail', type 'boolean'
    soql_element :disable_mentions_post_email, 'UserPreferencesDisableMentionsPostEmail'

    # Element for 'DisMentionsCommentEmail', type 'boolean'
    soql_element :dis_mentions_comment_email, 'UserPreferencesDisMentionsCommentEmail'

    # Element for 'HideCSNDesktopTask', type 'boolean'
    soql_element :hide_csn_desktop_task, 'UserPreferencesHideCSNDesktopTask'

    # Element for 'HideChatterOnboardingSplash', type 'boolean'
    soql_element :hide_chatter_onboarding_splash, 'UserPreferencesHideChatterOnboardingSplash'

    # Element for 'HideSecondChatterOnboardingSplash', type 'boolean'
    soql_element :hide_second_chatter_onboarding_splash, 'UserPreferencesHideSecondChatterOnboardingSplash'

    # Element for 'DisCommentAfterLikeEmail', type 'boolean'
    soql_element :dis_comment_after_like_email, 'UserPreferencesDisCommentAfterLikeEmail'

    # Element for 'DisableLikeEmail', type 'boolean'
    soql_element :disable_like_email, 'UserPreferencesDisableLikeEmail'

    # Element for 'SortFeedByComment', type 'boolean'
    soql_element :sort_feed_by_comment, 'UserPreferencesSortFeedByComment'

    # Element for 'DisableMessageEmail', type 'boolean'
    soql_element :disable_message_email, 'UserPreferencesDisableMessageEmail'

    # Element for 'JigsawListUser', type 'boolean'
    soql_element :jigsaw_list_user, 'UserPreferencesJigsawListUser'

    # Element for 'DisableBookmarkEmail', type 'boolean'
    soql_element :disable_bookmark_email, 'UserPreferencesDisableBookmarkEmail'

    # Element for 'DisableSharePostEmail', type 'boolean'
    soql_element :disable_share_post_email, 'UserPreferencesDisableSharePostEmail'

    # Element for 'EnableAutoSubForFeeds', type 'boolean'
    soql_element :enable_auto_sub_for_feeds, 'UserPreferencesEnableAutoSubForFeeds'

    # Element for 'DisableFileShareNotificationsForApi', type 'boolean'
    soql_element :disable_file_share_notifications_for_api, 'UserPreferencesDisableFileShareNotificationsForApi'

    # Element for 'ShowTitleToExternalUsers', type 'boolean'
    soql_element :show_title_to_external_users, 'UserPreferencesShowTitleToExternalUsers'

    # Element for 'ShowManagerToExternalUsers', type 'boolean'
    soql_element :show_manager_to_external_users, 'UserPreferencesShowManagerToExternalUsers'

    # Element for 'ShowEmailToExternalUsers', type 'boolean'
    soql_element :show_email_to_external_users, 'UserPreferencesShowEmailToExternalUsers'

    # Element for 'ShowWorkPhoneToExternalUsers', type 'boolean'
    soql_element :show_work_phone_to_external_users, 'UserPreferencesShowWorkPhoneToExternalUsers'

    # Element for 'ShowMobilePhoneToExternalUsers', type 'boolean'
    soql_element :show_mobile_phone_to_external_users, 'UserPreferencesShowMobilePhoneToExternalUsers'

    # Element for 'ShowFaxToExternalUsers', type 'boolean'
    soql_element :show_fax_to_external_users, 'UserPreferencesShowFaxToExternalUsers'

    # Element for 'ShowStreetAddressToExternalUsers', type 'boolean'
    soql_element :show_street_address_to_external_users, 'UserPreferencesShowStreetAddressToExternalUsers'

    # Element for 'ShowCityToExternalUsers', type 'boolean'
    soql_element :show_city_to_external_users, 'UserPreferencesShowCityToExternalUsers'

    # Element for 'ShowStateToExternalUsers', type 'boolean'
    soql_element :show_state_to_external_users, 'UserPreferencesShowStateToExternalUsers'

    # Element for 'ShowPostalCodeToExternalUsers', type 'boolean'
    soql_element :show_postal_code_to_external_users, 'UserPreferencesShowPostalCodeToExternalUsers'

    # Element for 'ShowCountryToExternalUsers', type 'boolean'
    soql_element :show_country_to_external_users, 'UserPreferencesShowCountryToExternalUsers'

    # Element for 'ShowProfilePicToGuestUsers', type 'boolean'
    soql_element :show_profile_pic_to_guest_users, 'UserPreferencesShowProfilePicToGuestUsers'

    # Element for 'ShowTitleToGuestUsers', type 'boolean'
    soql_element :show_title_to_guest_users, 'UserPreferencesShowTitleToGuestUsers'

    # Element for 'ShowCityToGuestUsers', type 'boolean'
    soql_element :show_city_to_guest_users, 'UserPreferencesShowCityToGuestUsers'

    # Element for 'ShowStateToGuestUsers', type 'boolean'
    soql_element :show_state_to_guest_users, 'UserPreferencesShowStateToGuestUsers'

    # Element for 'ShowPostalCodeToGuestUsers', type 'boolean'
    soql_element :show_postal_code_to_guest_users, 'UserPreferencesShowPostalCodeToGuestUsers'

    # Element for 'ShowCountryToGuestUsers', type 'boolean'
    soql_element :show_country_to_guest_users, 'UserPreferencesShowCountryToGuestUsers'

    # Element for 'DisableFeedbackEmail', type 'boolean'
    soql_element :disable_feedback_email, 'UserPreferencesDisableFeedbackEmail'

    # Element for 'DisableWorkEmail', type 'boolean'
    soql_element :disable_work_email, 'UserPreferencesDisableWorkEmail'

    # Element for 'PipelineViewHideHelpPopover', type 'boolean'
    soql_element :pipeline_view_hide_help_popover, 'UserPreferencesPipelineViewHideHelpPopover'

    # Element for 'HideS1BrowserUI', type 'boolean'
    soql_element :hide_s1_browser_ui, 'UserPreferencesHideS1BrowserUI'

    # Element for 'DisableEndorsementEmail', type 'boolean'
    soql_element :disable_endorsement_email, 'UserPreferencesDisableEndorsementEmail'

    # Element for 'PathAssistantCollapsed', type 'boolean'
    soql_element :path_assistant_collapsed, 'UserPreferencesPathAssistantCollapsed'

    # Element for 'CacheDiagnostics', type 'boolean'
    soql_element :cache_diagnostics, 'UserPreferencesCacheDiagnostics'

    # Element for 'ShowEmailToGuestUsers', type 'boolean'
    soql_element :show_email_to_guest_users, 'UserPreferencesShowEmailToGuestUsers'

    # Element for 'ShowManagerToGuestUsers', type 'boolean'
    soql_element :show_manager_to_guest_users, 'UserPreferencesShowManagerToGuestUsers'

    # Element for 'ShowWorkPhoneToGuestUsers', type 'boolean'
    soql_element :show_work_phone_to_guest_users, 'UserPreferencesShowWorkPhoneToGuestUsers'

    # Element for 'ShowMobilePhoneToGuestUsers', type 'boolean'
    soql_element :show_mobile_phone_to_guest_users, 'UserPreferencesShowMobilePhoneToGuestUsers'

    # Element for 'ShowFaxToGuestUsers', type 'boolean'
    soql_element :show_fax_to_guest_users, 'UserPreferencesShowFaxToGuestUsers'

    # Element for 'ShowStreetAddressToGuestUsers', type 'boolean'
    soql_element :show_street_address_to_guest_users, 'UserPreferencesShowStreetAddressToGuestUsers'

    # Element for 'LightningExperiencePreferred', type 'boolean'
    soql_element :lightning_experience_preferred, 'UserPreferencesLightningExperiencePreferred'

    # Element for 'PreviewLightning', type 'boolean'
    soql_element :preview_lightning, 'UserPreferencesPreviewLightning'

    # Element for 'HideEndUserOnboardingAssistantModal', type 'boolean'
    soql_element :hide_end_user_onboarding_assistant_modal, 'UserPreferencesHideEndUserOnboardingAssistantModal'

    # Element for 'HideLightningMigrationModal', type 'boolean'
    soql_element :hide_lightning_migration_modal, 'UserPreferencesHideLightningMigrationModal'

    # Element for 'HideSfxWelcomeMat', type 'boolean'
    soql_element :hide_sfx_welcome_mat, 'UserPreferencesHideSfxWelcomeMat'

    # Element for 'HideBiggerPhotoCallout', type 'boolean'
    soql_element :hide_bigger_photo_callout, 'UserPreferencesHideBiggerPhotoCallout'

    # Element for 'GlobalNavBarWTShown', type 'boolean'
    soql_element :global_nav_bar_wt_shown, 'UserPreferencesGlobalNavBarWTShown'

    # Element for 'GlobalNavGridMenuWTShown', type 'boolean'
    soql_element :global_nav_grid_menu_wt_shown, 'UserPreferencesGlobalNavGridMenuWTShown'

    # Element for 'CreateLEXAppsWTShown', type 'boolean'
    soql_element :create_lex_apps_wt_shown, 'UserPreferencesCreateLEXAppsWTShown'

    # Element for 'FavoritesWTShown', type 'boolean'
    soql_element :favorites_wt_shown, 'UserPreferencesFavoritesWTShown'

    # Element for 'RecordHomeSectionCollapseWTShown', type 'boolean'
    soql_element :record_home_section_collapse_wt_shown, 'UserPreferencesRecordHomeSectionCollapseWTShown'

    # Element for 'RecordHomeReservedWTShown', type 'boolean'
    soql_element :record_home_reserved_wt_shown, 'UserPreferencesRecordHomeReservedWTShown'

    # Element for 'FavoritesShowTopFavorites', type 'boolean'
    soql_element :favorites_show_top_favorites, 'UserPreferencesFavoritesShowTopFavorites'

    # Element for 'ExcludeMailAppAttachments', type 'boolean'
    soql_element :exclude_mail_app_attachments, 'UserPreferencesExcludeMailAppAttachments'

    # Element for 'SuppressTaskSFXReminders', type 'boolean'
    soql_element :suppress_task_sfx_reminders, 'UserPreferencesSuppressTaskSFXReminders'

    # Element for 'SuppressEventSFXReminders', type 'boolean'
    soql_element :suppress_event_sfx_reminders, 'UserPreferencesSuppressEventSFXReminders'

    # Element for 'PreviewCustomTheme', type 'boolean'
    soql_element :preview_custom_theme, 'UserPreferencesPreviewCustomTheme'

    # Element for 'HasCelebrationBadge', type 'boolean'
    soql_element :has_celebration_badge, 'UserPreferencesHasCelebrationBadge'

    # Element for 'UserDebugModePref', type 'boolean'
    soql_element :user_debug_mode_pref, 'UserPreferencesUserDebugModePref'

    # Element for 'NewLightningReportRunPageEnabled', type 'boolean'
    soql_element :new_lightning_report_run_page_enabled, 'UserPreferencesNewLightningReportRunPageEnabled'

    # Element for 'Contact ID', type 'reference'
    soql_element :contact_id, 'ContactId'

    # Element for 'Account ID', type 'reference'
    soql_element :account_id, 'AccountId'

    # Element for 'Call Center ID', type 'reference'
    soql_element :call_center_id, 'CallCenterId'

    # Element for 'Extension', type 'phone'
    soql_element :extension, 'Extension'

    # Element for 'SAML Federation ID', type 'string'
    soql_element :saml_federation_id, 'FederationIdentifier'

    # Element for 'About Me', type 'textarea'
    soql_element :about_me, 'AboutMe'

    # Element for 'Url for full-sized Photo', type 'url'
    soql_element :url_for_fullsized_photo, 'FullPhotoUrl'

    # Element for 'Photo', type 'url'
    soql_element :photo, 'SmallPhotoUrl'

    # Element for 'Show external indicator', type 'boolean'
    soql_element :show_external_indicator, 'IsExtIndicatorVisible'

    # Element for 'Out of office message', type 'string'
    soql_element :out_of_office_message, 'OutOfOfficeMessage'

    # Element for 'Url for medium profile photo', type 'url'
    soql_element :url_for_medium_profile_photo, 'MediumPhotoUrl'

    # Element for 'Chatter Email Highlights Frequency', type 'picklist'
    soql_element :chatter_email_highlights_frequency, 'DigestFrequency'

    # Element for 'Default Notification Frequency when Joining Groups', type 'picklist'
    soql_element :default_notification_frequency_when_joining_groups, 'DefaultGroupNotificationFrequency'

    # Element for 'Data.com Monthly Addition Limit', type 'int'
    soql_element :datacom_monthly_addition_limit, 'JigsawImportLimitOverride'

    # Element for 'Last Viewed Date', type 'datetime'
    soql_element :last_viewed_date, 'LastViewedDate'

    # Element for 'Last Referenced Date', type 'datetime'
    soql_element :last_referenced_date, 'LastReferencedDate'

    # Element for 'Url for banner photo', type 'url'
    soql_element :url_for_banner_photo, 'BannerPhotoUrl'

    # Element for 'Url for IOS banner photo', type 'url'
    soql_element :url_for_ios_banner_photo, 'SmallBannerPhotoUrl'

    # Element for 'Url for Android banner photo', type 'url'
    soql_element :url_for_android_banner_photo, 'MediumBannerPhotoUrl'

    # Element for 'Has Profile Photo', type 'boolean'
    soql_element :has_profile_photo, 'IsProfilePhotoActive'
  end
end
