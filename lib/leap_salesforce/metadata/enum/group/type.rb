# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Group, Type
class Group < SoqlData
  # Enumeration for Type
  module Type
    include SoqlEnum

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Type'
      end

      # @return [Array] List of values for Type
      def values
        []
      end

      attr_reader
    end
  end
end
