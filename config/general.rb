# frozen_string_literal: true

if LeapSalesforce.environment == 'uat'
  ENV['SF_USERNAME'] = ENV['UAT_USERNAME']
  ENV['SF_CONSUMER_KEY'] = ENV['UAT_CONSUMER_KEY']
end

# Add users to LeapSalesforce context. First user is the default
module LeapSalesforce
  Users.add User.new :admin, ENV['SF_USERNAME']
  Users.add User.new :cet, 'enquiry@test.email.com',
                     description: 'Enquiries team'
end
