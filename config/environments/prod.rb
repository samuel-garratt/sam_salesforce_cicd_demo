# Code specific to setting up the  environment

Soaspec::OAuth2.debug_oauth = false # Turn this true if you need debug authentication
Soaspec::SpecLogger.output_to_terminal = false # Turn this to true if you want to see API traffic on the terminal
