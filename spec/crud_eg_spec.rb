# frozen_string_literal: true
# This file demonstrates how simple creation, reading updating and deletion (CRUD)
# can be done through LeapSalesforce classes
# Depending on validation rules and mandatory fields this may or may not work

RSpec.describe 'Contact CRUD' do
  let(:uniq_value) { 'UNIQ FIRST NAME' }
  let(:updated_value) { 'New Value' }
  it 'Create using factory bot' do
    @contact = FactoryBot.create(:contact)
  end
  it 'Read contact by name' do
    FactoryBot.create(:contact, first_name: uniq_value)
    @contact = Contact.find(first_name: uniq_value)
    retrieved_first_name = @contact.first_name
    expect(retrieved_first_name).to eq uniq_value
  end
  it 'Updates contacts name' do
    @contact = FactoryBot.create(:contact, first_name: uniq_value)
    @contact.first_name = updated_value
    expect(@contact.find.first_name).to eq updated_value
  end
  after { @contact.delete } # Delete data after each action. Comment out if you want data to remain
end
